var express=require('express');
var app = express();

var pos={x:0,y:0,z:0};
//var objetos=[{figura:"Cubo",color:"Rojo",x:3,y:3,z:3},{figura:"Cubo",color:"Rojo",x:6,y:6,z:6},{figura:"Cubo",color:"Rojo",x:-5,y:-5,z:-5},{figura:"Cubo",color:"Rojo",x:-3,y:-3,z:-3}];
var objetos = [];
var id = 0;
var r=1;

app.use(function(req,res,next) {
        res.header("Access-Control-Allow-Origin","*");
        res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
        next();
});

// Devuelve todos los objetos
app.get('/test/', function(req,res) {
	console.log('Test!');
	res.jsonp(JSON.stringify(objetos));
});

// Añade un objeto, devuelve el ID
app.get('/add/:figura/:color/:x/:z', function(req,res) {
	id++;
	var objeto = new Object();
	objeto.id = id;
	objeto.figura = req.params.figura;
	objeto.color = req.params.color;
	objeto.x = req.params.x;
	objeto.z = req.params.z;
	objeto.grados = 0;
	objetos.push(objeto);
        console.log('Añadido');
	res.jsonp(id);
});

// Elimina un objeto
app.get('/del/:id', function(req,res) {
	console.log('Borrar : ' + req.params.id);
	for (var i = 0; i < objetos.length; i++) {		// Iterar en el array
		if (objetos[i].id == req.params.id){		// Buscar el objeto
			var index = objetos.indexOf(objetos[i]);// Obtener la posición en el array
			objetos.splice(index, 1);		// Borrarlo
		}
	}
	res.jsonp('deleted');
});

app.get('/move/:id/:x/:z', function(req,res) {
	for (var i = 0; i < objetos.length; i++) {
		if (objetos[i].id == req.params.id){
			objetos[i].x = req.params.x;
			objetos[i].z = req.params.z;
		}
	}
	console.log('Movido');
	res.jsonp('moved');
});

app.get('/grados/:id/:grados', function(req,res) {
        for (var i = 0; i < objetos.length; i++) {
                if (objetos[i].id == req.params.id){
			objetos[i].grados = req.params.grados;
                }
        }
	console.log('grados actualizados');
        res.jsonp('grados actualizados');
});
/*
GET /params -> {formas:[],colores[]}
Retorna un objeto JSON con 2 atributos.
“formas” es un array de formas posibles.
“colores” es un array con la paleta de colores que es posible seleccionar.

POST /init/posx/posy/forma/color -> id
Inicializa cliente. Si la inicializacion es exitosa, retorna un id > 0.
Si la posicion ya esta tomada, retorna id=0.

PUT /update/id/tiltx/tilty/tiltz
Actualiza datos del cliente VR

GET /scene -> [{pos,forma,color,tilt}]
Recupera datos del escenario 3D para redibujar en cliente VR

DELETE/id
Desconecta cliente del escenario 3D


*/


app.listen(8000, '172.16.1.46');
console.log("REST POS instalado en port 8000");
