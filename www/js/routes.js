angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('vR', {
    url: '/vr',
    templateUrl: 'templates/vR.html',
    controller: 'vRCtrl',
    controllerAs: 'vRCtrl',
    params: {figura:null,color:null,posX:null,posZ:null,servidor:null}
  })

  .state('inicio', {
    url: '/inicio',
    templateUrl: 'templates/inicio.html',
    controller: 'inicioCtrl',
    controllerAs: 'inicioCtrl'
  })

$urlRouterProvider.otherwise('/inicio')

});
