angular.module('app.controllers', ['app.config','ionic.native'])
  
.controller('vRCtrl', ['$scope', '$http', '$stateParams', 'Config', '$state', '$cordovaDeviceOrientation',
function ($scope, $http, $stateParams, Config, $state, $cordovaDeviceOrientation) {
	var self = this;
	self.color = $stateParams.color;
	self.figura = $stateParams.figura;
	self.posX = $stateParams.posX;
	self.posZ = $stateParams.posZ;
	self.servidor = $stateParams.servidor;
	console.log(self.color);
	console.log(self.figura);
	console.log('Servidor: ' + self.servidor);

	var objetos = [];
	var miID = -1;

	var vision = 0;

	var scene = new THREE.Scene();
	var camera = new THREE.PerspectiveCamera(90,window.innerWidth/window.innerHeight,1,10000);

	var renderer = new THREE.WebGLRenderer({antialias: true});
	renderer.setSize(window.innerWidth*0.8,window.innerHeight*0.8);
	renderer.shadowMap.enabled = true;
	renderer.shadowMap.type = THREE.PCFSoftShadowMap;
	renderer.gammaInput = true;
	renderer.gammaOutput = true;
	renderer.setClearColor( 0xffffff, 0);

	document.getElementById("contenidoVr").appendChild(renderer.domElement);

	//var axisHelper = new THREE.AxisHelper( 10 );
	//scene.add( axisHelper );

	hemiLight = new THREE.HemisphereLight( 0xffffff, 0xffffff, 0.6);
	hemiLight.color.setHSL( 0.6, 1 ,0.6 );
	hemiLight.groundColor.setHSL( 0.095, 1, 0.75 );
	hemiLight.position.set( 0,500,0 );
	scene.add(hemiLight);

	var groundGeo = new THREE.PlaneGeometry(6,6,0,0);
    	var groundMat = new THREE.MeshPhongMaterial( { color: 0xffffff, specular: 0x050505 } );
    	groundMat.color.setHSL( 0.095, 1, 0.75 );

	// Dust Particles
      	// http://aerotwist.com/tutorials/creating-particles-with-three-js/
      	// maybe try this instead? http://www.html5rocks.com/en/tutorials/casestudies/oz/

      	// create the particle variables
      	particleCount = 400;
      	var particles = new THREE.Geometry();
      	var pMaterial = new THREE.PointsMaterial({
            color: 0x7a5d43,
            size: 1
        });

      	// now create the individual particles
      	for (var p = 0; p < particleCount; p++) {

        	// create a particle with random
        	// position values, -250 -> 250
        	var pX = Math.random() * 500 - 250,
            	    pY = Math.random() * 300 - 150,
            	    pZ = Math.random() * 500 - 250,
            	particle = new THREE.Vector3(pX, pY, pZ);

        	// create a velocity vector
        	particle.velocity = new THREE.Vector3(
          	Math.random() * 5,              // x
          	0, // y: random vel
          	0);             // z

        	// add it to the geometry
        	particles.vertices.push(particle);
      	}

      	// create the particle system
      	particleSystem = new THREE.Points( particles, pMaterial );
	particleSystem.sortParticles = true;

      	// add it to the scene
      	scene.add( particleSystem );

	//LUCES
	var spotLight = new THREE.SpotLight( 0xffffff, 1 );
	spotLight.position.set( -14, 15, 4 );
	spotLight.castShadow = true;
	spotLight.angle = Math.PI / 4;
	spotLight.penumbra = 0.05;
	spotLight.decay = 2;
	spotLight.distance = 200;
	spotLight.shadow.mapSize.width = 1024;
	spotLight.shadow.mapSize.height = 1024;
	spotLight.shadow.camera.near = 1;
	spotLight.shadow.camera.far = 200;
	scene.add( spotLight );

	//NOS REGISTRAMOS
	 $http.get('http://' + self.servidor + ':8000/add/' + self.figura + "/" + self.color + "/" + self.posX + "/" + self.posZ)
                .success(function(data, status, headers, config) {
                        console.log('Mi ID : ' + data);
			miID = data;
                });

	// CARGAR OBJETOS
	var intervalID = setInterval(function() {
		$http.get('http://' + self.servidor + ':8000/test')
		.success(function(data, status, headers, config) {
			console.log('Data : ' + data);
			console.log('Longitud : ' + data.length);
			var datos = JSON.parse(data);

			for (i in objetos){
				scene.remove(objetos[i]);
			}
			objetos = [];

			for (i in datos){
				console.log(" -- i : " + i);
				if (datos[i].figura == "cubo"){
					shape1 = new THREE.Mesh(new THREE.BoxGeometry(1,1,1,1,1,1), new THREE.MeshLambertMaterial({color:parseInt(datos[i].color), wireframe:false}));
					shape1.castShadow = true;
					scene.add(shape1);
					objetos.push(shape1);
					shape1.position.set(datos[i].x,0.5,datos[i].z);
					shape1.rotateY(datos[i].grados);
				}
				if(datos[i].figura == "cono"){
                 			cono = new THREE.Mesh(new THREE.ConeGeometry(1,2,15), new THREE.MeshLambertMaterial({color:parseInt(datos[i].color,16), wireframe:false}));
                 			cono.castShadow = true;
					scene.add(cono);
                 			//X,Y;Z
                 			cono.position.set(datos[i].x,1,datos[i].z);
					cono.rotateY(datos[i].grados);
         			}
         			if(datos[i].figura == "rombo"){
               				var geometry = new THREE.CircleGeometry( 1, 4 );
                 			var material = new THREE.MeshBasicMaterial( { color: parseInt(datos[i].color,16) } );
                 			var rombo = new THREE.Mesh( geometry, material );
					rombo.castShadow = true;
                 			scene.add(rombo);
                 			//X,Y;Z
	                 		rombo.position.set(datos[i].x,1,datos[i].z);
					rombo.rotateY(datos[i].grados);
				}
			}
		});
			
	},1000);

    var ground = new THREE.Mesh( groundGeo, groundMat );
    ground.rotation.x = -Math.PI/2;
    ground.position.x = 3;
    ground.position.y = 0;
    ground.position.z = 3;
    scene.add( ground );

    ground.receiveShadow = true;
	
	camera.position.set(3,3,10);

	function render() {

	// ------------------------------------------------
      	particleSystem.rotation.y += 0.005;

      	var pCount = particleCount;
      	// while (pCount--) {
      	while (false) {

        	// get the particle
        	var particle = particleSystem.geometry.vertices[ pCount ];

        	// check if we need to reset
        	if (particle.x > 250) {
          		particle.x = -250;
          		// particle.velocity.x = 0;
        	}

        	// update the velocity with a splat of randomniz
        	// particle.velocity.x -= Math.random() * .1;

        	// and the position
        	particle.add( particle.velocity );
      	}

      	// flag to the particle system that we've changed its vertices.
      	particleSystem.geometry.verticesNeedUpdate = true;
      	// ------------------------------------------------

		requestAnimationFrame(render);
		renderer.render(scene,camera);
	//	effect.render(scene,camera);
	}

	render();

	$scope.moverCamaraArriba = function() {
		camera.position.y = camera.position.y + 2;
	}
	$scope.moverCamaraAbajo = function() {
		camera.position.y = camera.position.y - 2;
	}
	$scope.moverCamaraDerecha = function() {
		camera.position.x = camera.position.x + 2;
	}
	$scope.moverCamaraIzquierda = function() {
		camera.position.x = camera.position.x - 2;
	}
	$scope.moverCamaraAdelante = function() {
		camera.position.z = camera.position.z + 2;
	}
	$scope.moverCamaraAtras = function() {
		camera.position.z = camera.position.z - 2;
	}
	$scope.moverCamaraRotXPos = function() {
		camera.rotation.x = camera.rotation.x + 5;
	}
	$scope.moverCamaraRotXNeg = function() {
                camera.rotation.x = camera.rotation.x - 5;
        }

	// DESCONEXION
	$scope.desconectarse = function() {
		$http.get('http://' + self.servidor + ':8000/del/' + miID)
                .success(function(data, status, headers, config) {
                        console.log('Desconectado');
			$state.go('inicio');
                });
	}

	var intervalGrados = setInterval(function() {
		$cordovaDeviceOrientation.getCurrentHeading().then(function(result) {
       			var magneticHeading = result.magneticHeading;
	       		var trueHeading = result.trueHeading;
       			var accuracy = result.headingAccuracy;
       			var timeStamp = result.timestamp;
	        	//document.getElementById("datos").innerHTML=magneticHeading + " - " + trueHeading + " - " + accuracy + " - " + timeStamp;
			//camera.rotation.y = -magneticHeading * Math.PI / 180;
    			$http.get('http://' + self.servidor + ':8000/grados/' + miID + "/" + (-1 * magneticHeading))
                		.success(function(data, status, headers, config) {
					console.log('Actualizado');
                	});
		}, function(err) {
      			// An error occurred
    		}); 
	},100);


}])
   
.controller('inicioCtrl', ['$scope', '$stateParams', 'Config', '$state',
function ($scope, $stateParams, Config, $state) {
		var self = this;
		self.color;
		self.figura;
		self.servidor;
		self.posX;
		self.posZ;
		self.submit = function(){
			Config.ENV.SERVER_URL=self.servidor;
			$state.go('vR',
				{
					figura: self.figura,
					color: self.color,
					posX: self.posX,
					posZ: self.posZ,
					servidor: self.servidor
				});
		}
}])
 
